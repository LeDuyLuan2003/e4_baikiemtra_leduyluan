﻿using Microsoft.EntityFrameworkCore;

namespace E4_BaiKiemTra.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }

        public DbSet<Accounts> Accounts { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employees> Employees { get; set; }
        public DbSet<Logs> Logs { get; set; }
        public DbSet<Reports> Reports { get; set; }
        public DbSet<Transactions> Transactions { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Reports>()
                .HasOne(r => r.Log)
                .WithMany()
                .HasForeignKey(r => r.LogsID)
                .OnDelete(DeleteBehavior.NoAction); 
            modelBuilder.Entity<Reports>()
            .HasOne(r => r.Transaction)
            .WithMany()
            .HasForeignKey(r => r.TransactionID)
            .OnDelete(DeleteBehavior.NoAction);
            // Các cấu hình khác

            base.OnModelCreating(modelBuilder);
        }
    }
}
