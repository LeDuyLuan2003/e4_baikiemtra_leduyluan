﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace E4_BaiKiemTra.Models
{
    public class Transactions
    {
        [Key]
        public int TransactionID { get; set; }

        public int EmployeeID { get; set; }
        public Employees? Employee { get; set; }

        public int CustomerID { get; set; }
        public Customer? Customer { get; set; }
        public string Name { get; set; }

    }
}
