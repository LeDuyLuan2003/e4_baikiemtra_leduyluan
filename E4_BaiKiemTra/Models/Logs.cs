﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace E4_BaiKiemTra.Models
{
    public class Logs
    {
        [Key]
        public int LogsID { get; set; }
        public int TransactionID { get; set; }
        public Transactions? Transaction { get; set; }
        public DateTime Logindate { get; set; }
        public DateTime Logintime { get; set; }

    

    }
}
