﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace E4_BaiKiemTra.Models
{
    public class Accounts
    {
        [Key]
        public int AccountID { get; set; }
        public int CustomerID { get; set; }
        public Customer? Customer { get; set; }
        public string Accountname { get; set; }

    }
}
